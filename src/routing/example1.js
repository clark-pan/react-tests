import React, { Component } from 'react'
import { render } from 'react-dom'
import { browserHistory, Router, Route, IndexRedirect, Link } from 'react-router'
import Promise from 'bluebird'

require('bootstrap/dist/css/bootstrap.css')

class Page1 extends Component {
  constructor() {
    super();
    this.state = {
      inputText : ''
    }
  }
  componentWillMount() {
    this.props.updateLayoutActionItems(this.renderActionItems());
  }
  componentDidUpdate(prevProps, prevState) {
    if(prevState.inputText !== this.state.inputText) this.props.updateLayoutActionItems(this.renderActionItems());
  }
  renderActionItems() {
    return (
      <HighlightButton text={this.state.inputText} />
    );
  }

  render() {
    return (
      <div className="jumbotron">
        <h1>Page 1</h1>
        <input onChange={(event) => this.setState({ inputText : event.target.value })}/>
        {this.state.inputText}
        <div>the layout searched: {this.props.layoutSearchTerm}</div>
      </div>
    );
  }
}

class ApiCall {
  static getDocuments() {
    return Promise.timeout(2000).then(function(){
      return [
        { id : 1 },
        { id : 2 }
      ]
    });
  }
}

class HighlightButton extends Component {
  constructor() {
    super();
    this.state = {
      highlighted : false
    }
  }

  render() {
    return <div onClick={() => this.setState({highlighted : !this.state.highlighted})}>(highlighted: {this.state.highlighted.toString()}):{this.props.text}</div>
  }
}

class Page2 extends Component {
  componentWillMount() {
    this.props.updateLayoutActionItems(this.renderActionItems());
  }
  /*componentDidUpdate() {
    this.props.updateLayoutActionItems(this.renderActionItems());
  }*/
  renderActionItems() {
    return (
      <Link className="btn btn-primary btn-lg" to="page1" role="button">Action unique to page 2</Link>
    );
  }

  render() {
    return (
      <div className="jumbotron">
        <h1>Page 2</h1>
        <input />
      </div>
    )
  }
}

class DataRoomLayout extends Component {
  constructor() {
    super();
    this.state = {
      actionItems : null,
      searchTerm : ''
    };
  }
  updateLayoutActionItems(actionItems){
    this.setState({
      actionItems : actionItems
    });
  }

  render() {
    var props = this.props;
    var pageComponent = React.cloneElement(React.Children.only(props.children), {
      updateLayoutActionItems: this.updateLayoutActionItems.bind(this),
      layoutSearchTerm : this.state.searchTerm
    });
    return (
      <div>
        <div className="page-header">
          <h1>Header</h1>
          <Link className="btn btn-primary btn-lg" to="page1" role="button">Page 1</Link>
          <Link className="btn btn-primary btn-lg" to="page2" role="button">Page 2</Link>
          { this.state && this.state.actionItems || null }
          <input name="foo" placeholder="Search" onChange={(event) => this.setState({ searchTerm : event.target.value })} />
          What you searched for: { this.state.searchTerm }
        </div>
        <div>
          { pageComponent }
        </div>
      </div>
    );
  }
}

export default () => {
  render((
    <Router history={browserHistory}>
      <Route path="/">
        <Route path="" component={DataRoomLayout}>
          <IndexRedirect to="page1" />
          <Route path="page1" component={Page1} />
          <Route path="page2" component={Page2} />
        </Route>
      </Route>
    </Router>
  ), document.getElementById('react-app'));
}