import React, { Component } from 'react'
import { render } from 'react-dom'
import { Locations, Location, Link } from 'react-router-component'

require('bootstrap/dist/css/bootstrap.css')

class Page1 extends Component {
  renderActionItems() {
    return (
      <Link className="btn btn-primary btn-lg" to="page2" role="button">Action unique to page 1</Link>
    );
  }

  render() {
    return (
      <div className="jumbotron">
        <h1>Page 1</h1>
      </div>
    );
  }
}

class Page2 extends Component {
  renderActionItems() {
    return (
      <Link className="btn btn-primary btn-lg" to="page1" role="button">Action unique to page 2</Link>
    );
  }

  render() {
    return (
      <div className="jumbotron">
        <h1>Page 2</h1>
      </div>
    )
  }
}

class DataRoomLayout extends Component {
  componentWillMount() {
    this.getChildLocations();
  }

  getChildLocations() {
    setTimeout(() =>{ // Simulate require.ensure
      var onNavigation = () => {
        console.log(arguments);
      }
      this.setState({
        childLocations: (
          <Locations contextual onNavigation={onNavigation}>
            <Location path="/page1" handler={Page1} />
            <Location path="/page2" handler={Page2} />
          </Locations>
        )
      });
    }, 5000);
  }

  render() {
    return (
      <div>
        <div className="page-header">
          <h1>Header</h1>
          <Link className="btn btn-primary btn-lg" href="page1" role="button">Page 1</Link>
          <Link className="btn btn-primary btn-lg" href="page2" role="button">Page 2</Link>
          <input name="foo" placeholder="Search" />
        </div>
        <div>
          {
            (this.state && this.state.childLocations) ? this.state.childLocations : (<div>Loading</div>)
          }
        </div>
      </div>
    )
  }
}

class App extends Component {
  render() {
    return (
      <Locations>
        <Location path="/*" handler={DataRoomLayout} />
      </Locations>
    )
  }
}

export default () => {
  render(<App />, document.getElementById('react-app'));
}