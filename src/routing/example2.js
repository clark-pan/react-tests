import React, { Component } from 'react'
import { render } from 'react-dom'
import { browserHistory, Router, Route, IndexRedirect, Link } from 'react-router'

require('bootstrap/dist/css/bootstrap.css')

const Page1 = () => {
  var actions = (
    <Link className="btn btn-primary btn-lg" to="page2" role="button">Action unique to page 1</Link>
  );
  return (
    <DataRoomLayout key="header" actionItems={actions}>
      <div className="jumbotron">
        <h1>Page 1</h1>
      </div>
    </DataRoomLayout>
  )
}

const Page2 = () => {
  var actions = (
    <Link className="btn btn-primary btn-lg" to="page1" role="button">Action unique to page 2</Link>
  );
  return (
    <DataRoomLayout key="header" actionItems={actions}>
      <div className="jumbotron">
        <h1>Page 2</h1>
      </div>
    </DataRoomLayout>
  )
}

const App = (props) => {
  var child = React.Children.only(props.children);
  return (
    React.cloneElement(child, { key: 'page' })
  )
}

const DataRoomLayout = (props) => {
  return (
    <div>
      <div className="page-header">
        <h1>Header</h1>
        <Link className="btn btn-primary btn-lg" to="page1" role="button">Page 1</Link>
        <Link className="btn btn-primary btn-lg" to="page2" role="button">Page 2</Link>
        {props.actionItems}
        <input name="foo" placeholder="Search" />
      </div>
      <div>
        {props.children}
      </div>
    </div>
  );
}


export default () => {
  render((
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRedirect to="page1" />
        <Route path="page1" component={Page1} />
        <Route path="page2" component={Page2} />
      </Route>
    </Router>
  ), document.getElementById('react-app'));
}