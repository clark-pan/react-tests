// @flow
//$FlowFixMe
import _ from 'lodash';
import React, { Component, PureComponent } from 'react'
import { render } from 'react-dom'

import './example2.css'

type RowData = {
  height: number,
  sticky: boolean,
  cells: Array<CellData>
}

type CellData = {
  width: number,
  sticky: boolean,
  cell: React.Element<any>
}

const PASSIVE_CAPTURE = {
  capture: true,
  passive: true
}

const PASSIVE = {
  passive: true
}

class DataViewCell extends PureComponent {
  static defaultProps = {
    height: 0,
    sticky: false
  }
  props: {
    height: number,
    width: number,
    sticky: boolean,
    children?: React.Element<any>,
    className?: string
  }
  
  _el: HTMLElement
  
  render(){
    const className = this.props.className || '';
    return (
      <div ref={(el) => this._el = el} className={`table-cell ${className}`} style={{ width: this.props.width, height: this.props.height }}>
        { this.props.children }
      </div>
    );
  }
}

class DataViewRow extends PureComponent {
  static defaultProps = {
    sticky: false,
    offset: 0
  }
  
  props: {
    height: number,
    sticky: boolean,
    children?: React.Element<DataViewCell>,
    className?: string
  }
  
  _el: HTMLElement
  
  render(){
    let width = 0;
    const children = React.Children.map(this.props.children, (child, i) => {
      width += child.props.width;
      return React.cloneElement(child, { height: this.props.height });
    }).filter(_.identity);

    const className = this.props.className || '';
    return (
      <div ref={(el) => this._el = el} className={`table-row ${className}`} style={{ height: this.props.height, width: width }}>
        { children }
      </div>
    )
  }
}

class DataView extends PureComponent {
  
  _headerFixed: HTMLElement
  _header: HTMLElement
  _body: HTMLElement
  
  onWindowScroll = enqueueFrame(() => {
    if(window.scrollY > 100){
      this._headerFixed.classList.add('stuck');
      this._header.classList.add('stuck');
    } else {
      this._headerFixed.classList.remove('stuck');
      this._header.classList.remove('stuck');
    }
  });
  
  onBodyScroll = enqueueFrame(() => {
    this._header.scrollLeft = this._body.scrollLeft;
  });
  
  componentDidMount() {
    window.addEventListener('scroll', this.onWindowScroll);
    this._body.addEventListener('scroll', this.onBodyScroll);
  }
  
  componentWillUnmount() {
    window.removeEventListener('scroll', this.onWindowScroll);
    this._body.removeEventListener('scroll', this.onBodyScroll);
  }
  
  render(){
    let fixedRows = [], fixedRowFixedColumns = [], fixedColumns = [];
    const {
      children,
      ...rest
    } = this.props;
    
    const rows = React.Children.map(children, (row) => {
      const rowFixedChildren = [];
      const rowChildren = React.Children.map(row.props.children, (cell) => {
        if(cell.props.sticky) {
          const clonedCell = React.cloneElement(cell, { height: row.props.height });
          rowFixedChildren.push(clonedCell);
          return null;
        }
        
        return cell;
      }).filter(_.identity);
      
      const clonedRow = React.cloneElement(row, { children: rowChildren });
      if(clonedRow.props.sticky) {
        fixedRows.push(clonedRow);
        fixedRowFixedColumns = fixedRowFixedColumns.concat(rowFixedChildren);
        return null;
      }
      
      fixedColumns = fixedColumns.concat(rowFixedChildren);
      
      return clonedRow;
    }).filter(_.identity);
    
    return (
      <div className="table">
        <div className="table-header-fixedcolumns" ref={(el) => this._headerFixed = el}>
          { fixedRowFixedColumns }
        </div>
        <div className="table-header" ref={(el) => this._header = el}>
          { fixedRows }
        </div>
        <div className="table-body-fixedcolumns">
          { fixedColumns }
        </div>
        <div className="table-body" ref={(el) => this._body = el}>
          { rows }
        </div>
      </div>
    )
  }
}

function times<T>(n:number, fn:(n:number) => T):Array<T> {
  let result = [];
  for(let i = 0; i < n; i++){
    result.push(fn(i));
  }
  return result;
}

function findParent(el:Element, selector: string):?Element {
  let parent = el.parentElement;
  while(parent && !parent.matches(selector)){
    parent = parent.parentElement;
  }
  return parent;
}

function findScrollableParents(el:Element, includeHidden:boolean):Array<Element> {
  // Implement this function should allow for this component to work more efficiently
  // when its inside another scrolling element. Check ng-ansarada's dr-capped-repeat
  // `scrollParents` for a jquery version of it
  return [];
}

function enqueueFrame(fn) {
  let enqueued = false;
  let frameId = null;
  const callback = () => {
    if(enqueued) return;
    enqueued = true;
    frameId = window.requestAnimationFrame(() => {
      enqueued = false;
      fn.apply(this, arguments);
    });
  };
  callback.cancel = () => {
    window.cancelAnimationFrame(frameId);
  }
  return callback;
}

export default () => {
  const groups = times(10, (n) => {
    return {
      id: n
    };
  });
  const rows = times(50, (n) => {
    return {
      id: n
    };
  });
  render((
    <div id="page">
      <div id="header"></div>
      <div id="main">
        <div id="sidebar"></div>
        <div id="content">
          <DataView>
            <DataViewRow height={50} sticky={true} className="white">
              <DataViewCell width={200} sticky={true} className="white">
                
              </DataViewCell>
              {
                _.map(groups, (group) => (
                  <DataViewCell key={`group-${group.id}`} width={300}>
                    Group {group.id}
                  </DataViewCell>
                ))
              }
            </DataViewRow>
            <DataViewRow height={50} sticky={true} className="white">
              <DataViewCell width={200} sticky={true} className="white">
                Name
              </DataViewCell>
              {
                _.chain(groups)
                  .map((group) => ([
                    <DataViewCell key={`group-${group.id}-value1`} width={100}>
                      Type 1
                    </DataViewCell>,
                    <DataViewCell key={`group-${group.id}-value2`} width={100}>
                      Type 2
                    </DataViewCell>,
                    <DataViewCell key={`group-${group.id}-value3`} width={100}>
                      Type 3
                    </DataViewCell>
                  ]))
                  .flatten()
                  .value()
              }
            </DataViewRow>
            {
              _.chain(rows)
                .map((row) => (
                  <DataViewRow key={`row-${row.id}`} height={25}>
                    <DataViewCell width={200} sticky={true} className="white">
                      Row { row.id }
                    </DataViewCell>
                    {
                      _.chain(groups)
                        .map((group) => ([
                          <DataViewCell key={`row-${row.id}-group-${group.id}-value1`} width={100}>
                            Value 1
                          </DataViewCell>,
                          <DataViewCell key={`row-${row.id}-group-${group.id}-value2`} width={100}>
                            Value 2
                          </DataViewCell>,
                          <DataViewCell key={`row-${row.id}-group-${group.id}-value3`} width={100}>
                            Value 3
                          </DataViewCell>
                        ]))
                        .flatten()
                        .value()
                    }
                  </DataViewRow>
                ))
                .value()
            }
          </DataView>
        </div>
      </div>
      <div id="footer"></div>
    </div>
  ), document.getElementById('react-app'));
}