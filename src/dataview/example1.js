// @flow
/**
 * First attempt at doing this.
 * 
 * Notes:
 * 
 * - Tab order will be kept. This should make it much easier to do keyboard accessibility.
 * - Does not do horizontal virtual scrolling, only vertical. Could be extended to support this as well.
 * - The fixed columns lag behind the normal scrolling. Wierdly enough, it doens't
 * happen on Chrome on our large monitors, but it does happen on Chrome on the 
 * laptop monitor. It also happens consistently in Firefox.
 * 
 * We can use some visual trickery to hide this problem. Something like fade out
 * the fixed columns while its scrolling and fade them in after its stopped
 */
//$FlowFixMe
import _ from 'lodash';
import React, { Component, PureComponent } from 'react'
import { render } from 'react-dom'

import './example1.css'

type RowData = {
  height: number,
  sticky: boolean,
  cells: Array<CellData>
}

type CellData = {
  width: number,
  sticky: boolean,
  cell: React.Element<any>
}

const PASSIVE_CAPTURE = {
  capture: true,
  passive: true
}

const PASSIVE = {
  passive: true
}

class DataViewCell extends PureComponent {
  static defaultProps = {
    height: 0,
    offset: 0,
    sticky: false
  }
  props: {
    offset: number,
    height: number,
    width: number,
    sticky: boolean,
    children?: React.Element<any>,
    className?: string
  }
  
  _el: HTMLElement
  _scrollParent: ?Element
  
  constructor(){
    super();
  }
  
  onScroll = enqueueFrame(() => {
    if(this._scrollParent){
      this._el.style.left = `${this._scrollParent.scrollLeft + this.props.offset}px`;
    }
  });
  
  componentDidMount() {
    this._scrollParent = findParent(this._el, '.table-scroller');
    if(this.props.sticky){
      this._scrollParent && this._scrollParent.addEventListener('scroll', this.onScroll);
      this.onScroll();
    }
  }
  
  componentWillUnmount() {
    if(this._scrollParent) this._scrollParent.removeEventListener('scroll', this.onScroll);
    this.onScroll.cancel();
  }
  
  render(){
    const className = this.props.className || '';
    return (
      <div ref={(el) => this._el = el} className={`cell ${className}`} style={{ width: this.props.width, height: this.props.height, left: this.props.offset }}>
        { this.props.children }
      </div>
    );
  }
}

class DataViewRow extends PureComponent {
  static defaultProps = {
    sticky: false,
    offset: 0
  }
  
  props: {
    height: number,
    offset: number,
    sticky: boolean,
    children?: React.Element<DataViewCell>,
    className?: string
  }
  
  _el: HTMLElement
  _scrollParent: ?Element
  _stuck: boolean
  
  // the offset left of each of the items in the children
  // used to make the calculations during scrolling more performant
  // theres an extra number in there at the end for the right of the last item
  _childrenOffset: Array<number>
  
  constructor(){
    super();
  }
  
  _stuck = false
  
  toggleSticky = enqueueFrame(() => {
    if(this.props.sticky) {
      const scrollParent = this._scrollParent;
      if(!scrollParent) return;
      const boundingRect = scrollParent.getBoundingClientRect();
      const actualTop = boundingRect.top + this.props.offset;
      if(actualTop < this.props.offset){
        if(!this._stuck){
          this._stuck = true;
          this._el.classList.add('stuck');
          this._el.style.width = window.getComputedStyle(scrollParent).width;
          scrollParent.addEventListener('scroll', this.onParentScroll);
          this.onParentScroll();
        }
      } else {
        if(this._stuck){
          this._stuck = false;
          this._el.classList.remove('stuck');
          this._el.scrollLeft = 0;
          this._el.style.width = `${_.last(this._childrenOffset)}px`;
          scrollParent.removeEventListener('scroll', this.onParentScroll);
          this.onParentScroll.cancel();
        }
      }
      this._scrollParent = scrollParent;
    } else {
      if(this._scrollParent){
        this._scrollParent.removeEventListener('scroll', this.onParentScroll);
        this.onParentScroll.cancel();
        this._scrollParent = null;
      }
      if(this._stuck){
        this._stuck = false;
        this._el.scrollLeft = 0;
        this._el.style.width = `${_.last(this._childrenOffset)}px`;
        this._el.classList.remove('stuck');
      }
    }
  });
  
  onParentScroll = enqueueFrame(() => {
    const scrollParent = this._scrollParent;
    if(!scrollParent) return;
    this._el.scrollLeft = scrollParent.scrollLeft;
  });
  
  componentWillMount() {
    this._childrenOffset = this._calculateChildrenOffset(this.props.children);
  }
  
  componentDidMount(){
    this._scrollParent = findParent(this._el, '.table-scroller');
    window.addEventListener('scroll', this.toggleSticky, PASSIVE_CAPTURE);
    this.toggleSticky();
  }
  
  componentWillUpdate(nextProps){
    if(nextProps.children !== this.props.children){
      this._childrenOffset = this._calculateChildrenOffset(nextProps.children);
    }
  }
  
  componentWillUnmount(){
    const scrollParent = this._scrollParent;
    window.removeEventListener('scroll', this.toggleSticky, PASSIVE_CAPTURE);
    
    if(scrollParent){
      scrollParent.removeEventListener('scroll', this.onParentScroll);
    }
    
    this.onParentScroll.cancel();
    this.toggleSticky.cancel();
  }
  
  _calculateChildrenOffset(children){
    let left = 0;
    const offsets = React.Children.map(children || [], (child) => {
      const { width } = child.props;
      const offset = left;
      left += width;
      return offset;
    });
    offsets.push(left);
    return offsets;
  }
  
  render(){
    const children = React.Children.map(this.props.children, (child, i) => {
      const offset = this._childrenOffset[i];
      return React.cloneElement(child, { height: this.props.height, offset });
    }).filter(_.identity);

    const className = this.props.className || '';
    return (
      <div ref={(el) => this._el = el} className={`row ${className}`} style={{ width: _.last(this._childrenOffset), height: this.props.height, top: this.props.offset }}>
        { children }
      </div>
    )
  }
}

class DataView extends PureComponent {
  state: {
    cursor: [number, number]
  }
  
  props: {
    children?: React.Element<DataViewRow>
  }
  
  _scroller: Element
  
  // the offset top of each of the items in the children
  // used to make the calculations during scrolling more performant
  // theres an extra number in there at the end for the bottom of the last item
  _childrenOffset: Array<number>
  
  constructor(){
    super();
    
    this.state = {
      cursor: [0, 0]
    }
  }
  
  onScroll = enqueueFrame(() => {
    const boundingRect = this._scroller.getBoundingClientRect();
    // We exclude the documentElement since the window scroll calculations represents
    // the area that documentElement defines.
    const scrollParents = _.without(findScrollableParents(this._scroller, true), document.documentElement);
    const windowClientRect = {
      top: 0,
      left: 0,
      bottom: window.innerHeight,
      right: window.innerWidth
    };
    const visibleRect = _.chain(scrollParents)
      .map((el) => el.getBoundingClientRect())
      .unshift(windowClientRect)
      .reduce((acc, rect) => ({
        top: Math.max(acc.top, rect.top),
        bottom: Math.max(acc.bottom, rect.bottom)
      }))
      .value();
    
    const start = Math.max((boundingRect.top - visibleRect.top) * -1, 0);
    const end = start + (visibleRect.bottom - Math.max(boundingRect.top, visibleRect.top));
    const cursor = [
      _.findIndex(this._childrenOffset, (offset) => offset >= start) - 5,
      _.findLastIndex(this._childrenOffset, (offset) => offset <= end) + 5
    ];
    this.setState({
      cursor
    });
  });
  
  componentWillMount() {
    this._childrenOffset = this._calculateChildrenOffset(this.props.children);
  }
  
  componentDidMount(){
    window.addEventListener('scroll', this.onScroll, PASSIVE_CAPTURE);
  }
  
  componentWillUpdate(nextProps) {
    if(nextProps.children !== this.props.children){
      this._childrenOffset = this._calculateChildrenOffset(nextProps.children);
    }
  }
  
  componentWillUnmount(){
    window.removeEventListener('scroll', this.onScroll, PASSIVE_CAPTURE);
  }
  
  _calculateChildrenOffset(children){
    let top = 0;
    const offsets = React.Children.map(children || [], (child) => {
      const { height } = child.props;
      const offset = top;
      top += height;
      return offset;
    });
    offsets.push(top);
    return offsets;
  }
  
  render(){
    const [cursorStart, cursorEnd] = this.state.cursor;
    const children = React.Children.map(this.props.children, (child, i) => {
      if ( i > cursorEnd ) return null;
      if ( i < cursorStart && !child.props.sticky) return null;
      const offset = this._childrenOffset[i];
      return React.cloneElement(child, { offset });
    }).filter(_.identity);
    return (
      <div className="table">
        <div className="table-scroller" ref={(el) => this._scroller = el} style={{ height: _.last(this._childrenOffset) }}>
          { children }
        </div>
      </div>
    )
  }
}

function times<T>(n:number, fn:(n:number) => T):Array<T> {
  let result = [];
  for(let i = 0; i < n; i++){
    result.push(fn(i));
  }
  return result;
}

function findParent(el:Element, selector: string):?Element {
  let parent = el.parentElement;
  while(parent && !parent.matches(selector)){
    parent = parent.parentElement;
  }
  return parent;
}

function findScrollableParents(el:Element, includeHidden:boolean):Array<Element> {
  // Implement this function should allow for this component to work more efficiently
  // when its inside another scrolling element. Check ng-ansarada's dr-capped-repeat
  // `scrollParents` for a jquery version of it
  return [];
}

function enqueueFrame(fn) {
  let enqueued = false;
  let frameId = null;
  const callback = () => {
    if(enqueued) return;
    enqueued = true;
    frameId = window.requestAnimationFrame(() => {
      enqueued = false;
      fn.apply(this, arguments);
    });
  };
  callback.cancel = () => {
    window.cancelAnimationFrame(frameId);
  }
  return callback;
}

export default () => {
  const groups = times(10, (n) => {
    return {
      id: n
    };
  });
  const rows = times(5000, (n) => {
    return {
      id: n
    };
  });
  render((
    <div id="page">
      <div id="header"></div>
      <div id="main">
        <div id="sidebar"></div>
        <div id="content">
          <DataView>
            <DataViewRow height={50} sticky={true} className="white">
              <DataViewCell width={200} sticky={true} className="white">
                
              </DataViewCell>
              {
                _.map(groups, (group) => (
                  <DataViewCell key={`group-${group.id}`} width={300}>
                    Group {group.id}
                  </DataViewCell>
                ))
              }
            </DataViewRow>
            <DataViewRow height={50} sticky={true} className="white">
              <DataViewCell width={200} sticky={true} className="white">
                Name
              </DataViewCell>
              {
                _.chain(groups)
                  .map((group) => ([
                    <DataViewCell key={`group-${group.id}-value1`} width={100}>
                      Type 1
                    </DataViewCell>,
                    <DataViewCell key={`group-${group.id}-value2`} width={100}>
                      Type 2
                    </DataViewCell>,
                    <DataViewCell key={`group-${group.id}-value3`} width={100}>
                      Type 3
                    </DataViewCell>
                  ]))
                  .flatten()
                  .value()
              }
            </DataViewRow>
            {
              _.chain(rows)
                .map((row) => (
                  <DataViewRow key={`row-${row.id}`} height={25}>
                    <DataViewCell width={200} sticky={true} className="white">
                      Row { row.id }
                    </DataViewCell>
                    {
                      _.chain(groups)
                        .map((group) => ([
                          <DataViewCell key={`row-${row.id}-group-${group.id}-value1`} width={100}>
                            Value 1
                          </DataViewCell>,
                          <DataViewCell key={`row-${row.id}-group-${group.id}-value2`} width={100}>
                            Value 2
                          </DataViewCell>,
                          <DataViewCell key={`row-${row.id}-group-${group.id}-value3`} width={100}>
                            Value 3
                          </DataViewCell>
                        ]))
                        .flatten()
                        .value()
                    }
                  </DataViewRow>
                ))
                .value()
            }
          </DataView>
        </div>
      </div>
      <div id="footer"></div>
    </div>
  ), document.getElementById('react-app'));
}