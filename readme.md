# What is this

This is a spike to see the differences between various approaches to a problem.

Switch between the examples in `src/index.js` to see the different examples.

## src/routing

This explores the difference between two popular routing solutions in React.
Its purpose is to figure out a good way to have reuseable 'layouts' that can be
animated between during transitions. Other requirements include code splitting (via require.ensure)

- Example1 - Idiomatic way of using react-router. The layout is a separate parent route.
Pages hold their state, but are also passed in an update method to send messages to
the layout.
- Example2 - Alternate way of using react-router. The layout is a component inside
the page. Pages hold all state, and pass the state to the layout for it to render.
Has the problem that switching between pages means the DOM object for the layout is lost,
making it impossible to do animation between pages. Up-side is that the whole state
tree is made very obvious.
- Example3 - Uses react-router-component. Similar to Example1. Each Layout is responsible
for rendering and defining the routes of its children. Upside, less magic. Downside... harder
to see global route definition (this could be an upside really).

## src/dataview

This explores different ways to get a fixed header and fixed column component, combined
reducing the amount of DOM elements to only those visible in the DOM.

- Example1 - This is implemented using 'position:fixed' for the fixed rows, and changing the
left position of fixed columns as you scroll. It works well in the vertical direction, but horizontal
has scrolling performance problems. Learning here is that its not performant to update positioning,
as you scroll. More notes at the top of the example1.js

- Example2 - INCOMPLETE. Intended to be based off of http://w2ui.com/web/demos/#!grid/grid-27.
Traditional way of doing fixed headers and virtual scrolling is to put it into a 'box', rather
than going off of the whole window. Performance becomes less of a problem since you don't need
to update the position of the cells on scroll, but the effect is slightly different.

Also, because you need to completely change the DOM order to do this effect,
tab order is wrong as well. Instead of going left to right and then top to bottom,
it will cycle top to bottom for the fixed columns, then left to right on the body.

More hopeful approach, since we can intercept tabbing to fix the tab order if we want.

Its incomplete because i've hard coded lots of it, and it doesn't implement the partial view thingy.

# Getting started

Download [reactpack](https://github.com/olahol/reactpack)
run
```
npm install
reactpack -d src/index.js
```